const template2 = document.createElement('template');

template2.innerHTML = `
  <style>
    .image{ 
      height: 100%; 
      width: 100%;
      cursor: pointer;
    }
  </style>
  <a class="link">
    <img class="image" />
  </a>
`

class CTA extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template2.content.cloneNode(true));
    this.shadowRoot.querySelector('.image').setAttribute('src', this.getAttribute('image'));
    this.shadowRoot.querySelector('.link').setAttribute('href', this.getAttribute('url'));
  }

  connectedCallback() {
    this.shadowRoot.querySelector('.link').addEventListener('click', (e) => {
      e.preventDefault();
      window.open(this.getAttribute('url'), '_blank');
    });
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('.link').removeEventListener();
  }
}


window.customElements.define('route-cta', CTA);