const template = document.createElement('template');
template.innerHTML = `
  <style>
    .wrapper {
      height: 400px; 
      width: 600px;
      position: relative;
      overflow: hidden;
      cursor: pointer;
    }

    .container {
      background-color: white;
    }

    .darkmode {
      color: white;
      background-color: black;
    }

    .stack {
      position: absolute
    }

    .cta {
      overflow: hidden;
      height: 140px;
      width: 140px;
      margin: 4px;
      border-radius: 12px;
      right: 0;
    }

    .info {
      top: 4px; 
      left: 4px; 
      padding: 12px; 
      width: 120px; 
      border-radius: 12px;
      overflow: hidden;
    }

    .fullsize {
      height: 100%; 
      width: 100%;
    }
  </style>

  <div class="wrapper">
    <div class="stack fullsize">
      <slot name="map"></slot>
    </div>

    <div class="container stack info" style=""></div>

    <div class="stack cta">
      <slot name="cta"></slot>
    </div>
  </div>
  
`

class RoutePackageTracker extends HTMLElement {
  constructor() {
    super();
    this.darkmode = this.hasAttribute("darkmode")
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    if(this.hasAttribute("darkmode")){
      this.shadowRoot.querySelector('.container').classList.add("darkmode");
    }
    this.shadowRoot.querySelector('.container').innerText = `Hello ${this.getAttribute("name")}, your order is getting ready!`;
  }

  toggleDarkMode() {
    this.darkmode = !this.darkmode;
    if(this.darkmode){
      this.shadowRoot.querySelector('.container').classList.add("darkmode");
    } else {
      this.shadowRoot.querySelector('.container').classList.remove("darkmode");
    }
  }

  connectedCallback() {
    this.shadowRoot.querySelector('.wrapper').addEventListener('click', (e) => {
      this.toggleDarkMode();
    })
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector('.wrapper').removeEventListener()
  }
}

window.customElements.define('route-package-tracker', RoutePackageTracker);